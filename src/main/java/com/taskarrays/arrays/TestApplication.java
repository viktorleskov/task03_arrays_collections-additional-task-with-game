package com.taskarrays.arrays;

public class TestApplication {
    final static int masval = 5;

    public static void main(String[] args) {
        Test1(); //Product test
        Test2(); //Fraction test
        Test3(); //Delete series test
        Test4(); //Delete doubled elements test
    }
    public static void Test1(){
        int[] first = ArraysAction.getRandomMas(masval); //from 0 to 50
        int[] second = ArraysAction.getRandomMas(masval);
        int[] third = ArraysAction.getProductOfTwoArrays(first, second);
        printArr(first);
        printArr(second);
        System.out.println("Product test");
        printArr(third);
        printLine();
    }
    public static void Test2(){
        int[] first = ArraysAction.getRandomMas(masval);
        int[] second = ArraysAction.getRandomMas(masval);
        int[] third = ArraysAction.getFractionOfTwoArrays(first, second);
        printArr(first);
        printArr(second);
        System.out.println("Fraction test");
        printArr(third);
        printLine();
    }
    public static void Test3(){
        int[] first = new int[7];
        first[0]=2;
        first[1]=2;
        first[2]=2;
        first[3]=1;
        first[4]=3;
        first[5]=4;
        first[6]=4;

        printArr(first);
        first=ArraysAction.clearArrayFromElementSeries(first);
        System.out.println("Delete series test");
        printArr(first);
        printLine();
    }
    public static void Test4(){
//        int[] first = ArraysAction.getRandomMas(masval);
        int[] first = new int[7];
        first[0]=2;
        first[1]=2;
        first[2]=2;
        first[3]=1;
        first[4]=3;
        first[5]=4;
        first[6]=4;
        printArr(first);
        System.out.println("Delete all doubled elements test");
        first = ArraysAction.clearArrayFromDoubleElements(first);
        printArr(first);
        printLine();
    }
    private static void printArr(int[] array){
        System.out.println(ArraysAction.toString(array));
    }
    private static void printLine(){
        System.out.println("-------------------------------------");
    }
}

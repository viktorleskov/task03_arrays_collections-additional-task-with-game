package com.taskarrays.arrays;

import java.util.Random;

public class ArraysAction {
    private ArraysAction(){}
    public static int[] getRandomMas(int len) {
        Random rand = new Random();
        int[] array = new int[len];
        for (int i = 0; i < len; i++) {
            array[i] = rand.nextInt(15);
        }
        return array;
    }
    public static int[] getProductOfTwoArrays(int[] first, int[] second){
        int[] third = new int[getValueOfArraysSimilarity(first,second)];
        for(int k=0; k<second.length; k++){
            if(hasThisArrayThatElement(first,second[k])){
                third=putOneElementToArray(third,second[k]);
            }
        }
        third=clearArrayZeroElements(third);
        return third;
    }
    public static int[] getFractionOfTwoArrays(int[] first, int[] second){
        int[] third = new int[0];
        for(int k=0; k<second.length; k++){
            if(!hasThisArrayThatElement(first,second[k])){
                third=putOneElementToArray(third,second[k]);
            }
        }
        for(int k=0; k<first.length; k++){
            if(!hasThisArrayThatElement(second,first[k])){
                third=putOneElementToArray(third,first[k]);
            }
        }
        third=clearArrayZeroElements(third);
        return third;
    }
    public static int[] clearArrayFromElementSeries(int[] array){
        int[] newarray = new int[array.length];
        copyArray(array,newarray);
        for(int k=0; k < newarray.length;k++){
            if(k+1 < newarray.length){
                if(newarray[k]==newarray[k+1]){
                    newarray=deleteOneElementFromArray(newarray,k);
                    k--; //0
                }
            }
        }
        return newarray;
    }
    public static int[] clearArrayFromDoubleElements(int[] array){
        int[] newarray = new int[array.length];
        copyArray(array, newarray);
        int[] indexList;
        for(int k=0; k<newarray.length; k++){
            if((indexList=getIndexListOfArrayElement(newarray, newarray[k])).length > 1){
                newarray=deleteElementsFromArrayByIndexList(newarray,indexList);
            }
        }
        return newarray;
    }
    public static String toString(int[] array){
        return "{  "+toStringNext(array,0);
    }
    //-------------------------------Private methods(eeea)


    private static String toStringNext(int[] array, int index){
        if(index==array.length)return "\b\b  }";
        return ""+array[index]+", "+toStringNext(array,index+1);
    }
    private static int getValueOfArraysSimilarity(int[] first, int[] second){
        int value=0;
        for(int k=0; k<first.length; k++){
            for(int i=0; i<second.length; i++){
                if(first[k]==second[i])value++;
            }
        }
        return value;
    }
    private static void copyArray(int[] from, int[] to){
        for(int k=0; k<from.length; k++){
            to[k]=from[k];
        }
    }
    private static boolean hasThisArrayThatElement(int[]array, int element){
        for(int has:array){
            if(has==element){
                return true;
            }
        }
        return false;
    }
    private static int[] getIndexListOfArrayElement(int[] array, int element){
        int[] indexList = new int[0];
        for(int k=0; k<array.length; k++){
            if(array[k]==element){
                indexList=putOneElementToArray(indexList,k);
            }
        }
        return indexList;
    }
    private static int[] deleteOneElementFromArray(int[] array, int index){
        int[] newmas = new int[array.length-1];
        for(int k=0, diff=0; k<array.length; k++){
            if(k==index){
                diff--;
                continue;
            }
            newmas[k+diff]=array[k];
        }
        return newmas;
    }
    private static int[] deleteElementsFromArrayByIndexList(int[] array, int[] indexList){
        int[] newarray = new int[array.length];
        copyArray(array,newarray);
        for(int k=0; k<indexList.length; k++){
            newarray = deleteOneElementFromArray(newarray,indexList[k]-k);
        }
        return newarray;
    }
    private static int[] clearArrayZeroElements(int[] array){
        int[] newmas = new int[array.length];
        copyArray(array,newmas);
        for(int k=0; k<newmas.length; k++){
            if(newmas[k]==0) {
                newmas=deleteOneElementFromArray(newmas,k);
                k--;
            }
        }
        return newmas;
    }
    private static int[] putOneElementToArray(int[] array, int element){
        int[] newmas = new int[array.length+1];
        copyArray(array,newmas);
        newmas[array.length]=element;
        return newmas;
    }

}

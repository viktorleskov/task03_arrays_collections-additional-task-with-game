package com.taskarrays.gametask.utils;

@FunctionalInterface
public interface Task {
    void execute();
}

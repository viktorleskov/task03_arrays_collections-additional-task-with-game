package com.taskarrays.gametask.utils;

@FunctionalInterface
public interface Printable {
    void print();
}

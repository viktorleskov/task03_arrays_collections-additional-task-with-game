package com.taskarrays.gametask;

import com.taskarrays.gametask.view.GameView;

public class Application {
    public static void main(String[] args) {
        GameView.handle().show(); }
}

package com.taskarrays.gametask.managers;

import com.taskarrays.gametask.utils.ButtonAction;
import com.taskarrays.gametask.utils.Task;
import com.taskarrays.gametask.view.GameView;

public abstract class ViewManager{
    private static ViewManager singleton;
    private ViewManager(){}
    private void setController(ViewManager singleton){
        ViewManager.singleton=singleton;
    }
    private ViewManager getController(){
        return singleton;
    }
    public static ViewManager handle(){
        if(singleton!=null)return singleton.getController();
        ViewManager singleton = new ViewManager() {};
        singleton.setController(singleton);
        return singleton.getController();
    }
    public Task ask(ButtonAction action){
        switch (action){
            case PRINTALLDOORS:return this::printAllDoors;
            case FORHOWMANYDOORSDEATHAWAITS:return this::forHowManyDoorsDeathAwaits;
            case HOWTOWIN:return this::howToWin;
            case REGENERATEDOORSARRAY:return this::regenerateDoorsArray;
            default:break;
        };
        return null;
    }
    private void printAllDoors(){
        GameView.handle().printToConsole(ModelManager.handle().getAllDoorsInfo());
    }
    private void forHowManyDoorsDeathAwaits(){
        GameView.handle().printToConsole(ModelManager.handle().forHowManyDoorsDeathAwaits());
    }
    private void howToWin(){
        GameView.handle().printToConsole(ModelManager.handle().howToWinThisGame());
    }
    private void regenerateDoorsArray(){
        ModelManager.handle().regenerateDoorsArray();
    }
}

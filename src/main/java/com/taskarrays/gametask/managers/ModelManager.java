package com.taskarrays.gametask.managers;

import com.taskarrays.gametask.model.ModelDataSet;

public abstract class ModelManager{
    private static ModelManager singleton;
    private ModelManager(){}
    private void setModelManager(ModelManager singleton){
        ModelManager.singleton=singleton;
    }
    private ModelManager getModelManager(){
        return singleton;
    }
    public static ModelManager handle() {
        if (singleton != null) return singleton.getModelManager();
        ModelManager singleton = new ModelManager() {};
        singleton.setModelManager(singleton);
        return singleton.getModelManager();
    }
    public void regenerateDoorsArray(){
        ModelDataSet.handle().regenerateDoorsArray();
    }
    public String getAllDoorsInfo(){
        return ModelDataSet.handle().toString();
    }
    public String forHowManyDoorsDeathAwaits(){
        return "For "+ModelDataSet.handle().calculateForHowManyDoorsDeathAwaits()+" doors death awaits.";
    }
    public String howToWinThisGame(){
        return ModelDataSet.handle().calculateOrderToWin();
    }
}

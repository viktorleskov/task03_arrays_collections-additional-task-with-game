package com.taskarrays.gametask.model;

import java.util.Random;

public class ModelDataSet {
    private static int VALUE_OF_DOORS=10;
    private static ModelDataSet singleton;
    private int PlayerHealthPoint;
    private int[] Doors;
    private ModelDataSet(){}
    private ModelDataSet getModelDataSet(){
        return singleton;
    }
    private void setModelDataSet(ModelDataSet singleton){
        ModelDataSet.singleton = singleton;
    }
    private int getRandomBehindTheDoor(){
        Random rand = new Random();
        return rand.nextInt(2)!=0? 10+rand.nextInt(70):-5-rand.nextInt(95);
    }
    private String toStringNext(int index){
        if(index>=Doors.length)return "\b\b  }";
        return ""+(index+1)+":"+(Doors[index]>0?"Artifact:+"+Doors[index]+", ":"Monster:"+Doors[index]+" hp, ")
                + toStringNext(index+1);
    }
    private int calculateWinNext(int index){
        if(index>=Doors.length)return 0;
        return (PlayerHealthPoint<-Doors[index]?1:0)+ calculateWinNext(index+1);
    }
    private String calculateOrderNext(int index){
        for(int k=index;k>-100;k--){
             if(hasDoorsArrayThatElement(k)){
                 return (k>0?"Artifact:":"Monster:")+(getElementIndexFromArray(k)+1)+", "+calculateOrderNext(k-1);
             }
        }
        return " -End.";
    }
    private int getElementIndexFromArray(int element){
        for(int k=0; k<Doors.length; k++){
            if(Doors[k]==element)return k;
        }
        return -1;
    }
    private boolean hasDoorsArrayThatElement(int element){
        for(int has:Doors){
            if(has==element){
                return true;
            }
        }
        return false;
    }
    public static ModelDataSet handle(){
        if(singleton!=null)return singleton.getModelDataSet();
        ModelDataSet singleton = new ModelDataSet(){};
        singleton.setModelDataSet(singleton);
        singleton.regenerateDoorsArray();
        return singleton.getModelDataSet();
    }
    public void regenerateDoorsArray(){
        Doors = new int[VALUE_OF_DOORS];
        for(int k=0;k<Doors.length;k++){
            Doors[k]=getRandomBehindTheDoor();
        }
    }
    public int calculateForHowManyDoorsDeathAwaits(){
        return calculateWinNext(0);
    }
    public String calculateOrderToWin(){
        return "Order for win: "+calculateOrderNext(80);
    }
    public String toString(){
        return "{ "+toStringNext(0);
    }

}

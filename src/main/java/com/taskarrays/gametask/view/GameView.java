package com.taskarrays.gametask.view;

import com.taskarrays.gametask.utils.ButtonAction;
import com.taskarrays.gametask.managers.ViewManager;
import com.taskarrays.gametask.utils.Printable;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public abstract class GameView{
    private static GameView singleton;
    private static Scanner input = new Scanner(System.in);
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;

    private GameView() {
        menu = new LinkedHashMap<>();
        menu.put("1", "  1 - print all doors");
        menu.put("2", "  2 - for how many doors death awaits");
        menu.put("3", "  3 - how to win");
        menu.put("4", "  4 - regenerate doors array");
        menu.put("Q", "  Q - exit");

        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", this::pressButton1);
        methodsMenu.put("2", this::pressButton2);
        methodsMenu.put("3", this::pressButton3);
        methodsMenu.put("4", this::pressButton4);
    }
    private GameView getView(){
        return singleton;
    }
    private void setView(GameView singleton){
        GameView.singleton = singleton;
    }
    private void pressButton1() {
        ViewManager.handle().ask(ButtonAction.PRINTALLDOORS).execute();
    }
    private void pressButton2() {
       ViewManager.handle().ask(ButtonAction.FORHOWMANYDOORSDEATHAWAITS).execute();
    }
    private void pressButton3() {
        ViewManager.handle().ask(ButtonAction.HOWTOWIN).execute();
    }
    private void pressButton4() {
        ViewManager.handle().ask(ButtonAction.REGENERATEDOORSARRAY).execute();
    }
    private void outputMenu() {
        printToConsole("\nMENU:");
        for (String str : menu.values()) {
            printToConsole(str);
        }
    }
    public static GameView handle(){
        if(singleton!=null)return singleton.getView();
        GameView singleton = new GameView(){};
        singleton.setView(singleton);
        return singleton.getView();
    }
    public void show() {
        String keyMenu;
        do {
            outputMenu();
            printToConsole("Please, select menu point.");
            keyMenu = input.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).print();
            } catch (Exception e) {
            }
        } while (!keyMenu.equals("Q"));
    }
    public void printToConsole(String s){
        System.out.println(s);
    }
}
